var fs = require('fs-extra');
var cc = require('closure-compiler');
var glob = require('glob');
var CleanCSS = require('clean-css');
var _ = require('lodash');
var handlebars = require('handlebars');
var jade = require('jade');

function emptyDirDir(dir, cb) {
    console.log('Emptying started...');
    fs.emptyDir(dir, function () {
        console.log('Emptying finished!');
        cb();
    });
}

function copyResources(from, to, cb) {
    console.log('Copying started...');
    fs.copy(from, to, function () {
        console.log('Copying finished!');
        cb();
    });
}

function compileTemplates(cb) {
    function compilingLodash(cb) {
        console.log('    Compiling lodash templates started...');
        var templates = glob.sync('./dist/**/*.lodash'), processed = 0;
        templates.forEach(function (file) {
            console.log('        Compiling lodash templates start: ' + file);
            fs.readFile(file, 'utf8', function (err, content) {
                fs.writeFile(file + '.js', 'define(["libs/lodash"], function(_) {return ' + _.template(content).source + '})', 'utf8', function () {
                    console.log('        Compiling lodash templates finished: ' + file);
                    if (++processed == templates.length) {
                        console.log('    Compiling lodash templates finished!');
                        cb();
                    }
                });
            });
        });
    }

    function compilingHandlebars(cb) {
        console.log('    Compiling handlebars templates started...');
        var templates = glob.sync('./dist/**/*.handlebars'), processed = 0;
        templates.forEach(function (file) {
            console.log('        Compiling handlebars templates start: ' + file);
            fs.readFile(file, 'utf8', function (err, content) {
                fs.writeFile(file + '.js', 'define(["libs/handlebars.runtime"], function(Handlebars) {return Handlebars.template(' + handlebars.precompile(content) + ')})', 'utf8', function () {
                    console.log('        Compiling handlebars templates finished: ' + file);
                    if (++processed == templates.length) {
                        console.log('    Compiling handlebars templates finished!');
                        cb();
                    }
                });
            });
        });
    }

    function compilingJade(cb) {
        console.log('    Compiling jade templates started...');
        var templates = glob.sync('./dist/**/*.jade'), processed = 0;
        templates.forEach(function (file) {
            console.log('        Compiling jade templates start: ' + file);
            fs.readFile(file, 'utf8', function (err, content) {
                fs.writeFile(file + '.js', 'define(["libs/jade"], function(jade) {return ' + jade.compileClient(content) + '})', 'utf8', function () {
                    console.log('        Compiling jade templates finished: ' + file);
                    if (++processed == templates.length) {
                        console.log('    Compiling jade templates finished!');
                        cb();
                    }
                });
            });
        });
    }

    compilingLodash(function () {
        compilingHandlebars(function () {
            compilingJade(cb);
        });
    });
}

function copyNodeModules(cb) {
    console.log('Copying node modules started...');
    var modules = [
        './node_modules/requirejs/require.js',
        './node_modules/jquery/dist/jquery.js',
        './node_modules/lodash/lodash.js',
        './node_modules/handlebars/dist/handlebars.runtime.js',
        {from: './node_modules/jade/runtime.js', to: 'jade.js'}
    ], modulesProcessed = 0;
    modules.forEach(function (module) {
        var from = module.from || module;

        console.log('    Copying node modules start: ' + from);
        fs.copy(from, './dist/js/libs/' + (module.to ? module.to : module.substr(module.lastIndexOf('/'))), function () {
            console.log('    Copying node modules finished: ' + from);
            if (++modulesProcessed == modules.length) {
                console.log('Copying node modules finished!');
                cb();
            }
        });
    });
}

function closureCompiler(cb) {
    console.log('CC started...');
    var jsFiles = glob.sync('./dist/**/*.js'), processed = 0;
    jsFiles.forEach(function (file) {
        console.log('    CC start: ' + file);
        cc.compile(fs.readFileSync(file), {}, function (err, stdout) {
            fs.writeFile(file, stdout, function () {
                console.log('    CC finished: ' + file);
                if (++processed == jsFiles.length) {
                    console.log('CC finished!');
                    cb();
                }
            });
        });
    });
}

function compressJSON(cb) {
    console.log('Compressing JSON started...');
    var jsonFiles = glob.sync('./dist/**/*.json'), processed = 0;
    jsonFiles.forEach(function (file) {
        console.log('    Compressing JSON start: ' + file);
        fs.readJSON(file, function (err, json) {
            fs.writeJSON(file, json, {spaces: null}, function () {
                console.log('    Compressing JSON finished: ' + file);
                if (++processed == jsonFiles.length) {
                    console.log('Compressing JSON finished!');
                    cb();
                }
            });
        });
    });
}

function clearCSS(cb) {
    console.log('Clearing CSS started...');
    var cssFiles = glob.sync('./dist/**/*.css'), processed = 0;
    cssFiles.forEach(function (file) {
        console.log('    Clearing CSS start: ' + file);
        fs.readFile(file, function (err, content) {
            new CleanCSS().minify(content, function (errors, minified) {
                fs.writeFile(file, minified.styles, function () {
                    console.log('    Clearing CSS finished: ' + file);
                    if (++processed == cssFiles.length) {
                        console.log('Clearing CSS finished!');
                        cb();
                    }
                });
            });
        });
    });
}

function build() {
    console.log('build started...');

    emptyDirDir('./dist', function () {
            copyResources('./src', './dist', function () {
                    compileTemplates(function () {
                            copyNodeModules(function () {
                                    closureCompiler(function () {
                                            compressJSON(function () {
                                                    clearCSS(function () {
                                                            console.log('build finished!');
                                                        }
                                                    )
                                                }
                                            )
                                        }
                                    )
                                }
                            )
                        }
                    )
                }
            )
        }
    );
}

build();